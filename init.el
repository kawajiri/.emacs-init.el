;; elpa: emacs package management
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

;; el-get:
;; (add-to-list 'load-path "~/.emacs.d/el-get/el-get")
;; (el-get 'sync)

;; generic custom command
(global-set-key "\M-g" 'goto-line)

;; indent tab
(setq-default indent-tabs-mode nil)
(put 'set-goal-column 'disabled nil)


;; company: complete anything
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;; company: irony
(require 'company-irony)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

;; (optional) adds CC special commands to `company-begin-commands' in order to
;; trigger completion at interesting places, such as after scope operator
;;     std::|
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)


;; auto-complete
(require 'auto-complete-config)
(ac-config-default)

;; irony-mode
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)

;; replace the `completion-at-point' and `complete-symbol' bindings in
;; irony-mode's buffers by irony-mode's function
(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(add-to-list 'load-path "~/.emacs.d/el-get/ac-irony/")
(require 'ac-irony)
(defun my-ac-irony-setup ()
  ;; be cautious, if yas is not enabled before (auto-complete-mode 1), overlays
  ;; *may* persist after an expansion.
  ;; (yas-minor-mode 1)
  (auto-complete-mode 1)

  (add-to-list 'ac-sources 'ac-source-irony)
  (define-key irony-mode-map (kbd "M-RET") 'ac-complete-irony-async))
(add-hook 'irony-mode-hook 'my-ac-irony-setup)


;; c++ related
(require 'google-c-style)
(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'google-make-newline-indent)

(add-hook 'c++-mode-hook
          '(lambda ()
             (c-set-style "gnu")
             (c-set-offset 'innamespace 0)
             (setq indent-tabs-mode nil)
             ))

;; auctex: userful commands
;; (load "auctex.el" nil t t)
;; (load "preview-latex.el" nil t t)

;; flyspell-mode: spell chekcer
(setq-default ispell-program-name "/opt/local/bin/aspell") ;; path to aspell
(setq flyspell-issu-welcome-flag nil)
(add-hook 'latex-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)

;; for waf: recognize wscript file as python script
(setq auto-mode-alist (cons '("wscript" . python-mode) auto-mode-alist))
